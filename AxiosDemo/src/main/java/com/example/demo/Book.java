package com.example.demo;

import lombok.Data;

@Data
public class Book {
    private int id;
    private String bookName;
    private String author;

    public Book(int id, String bookName,String author) {
        super();
        this.id = id;
        this.bookName = bookName;
    }

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}

