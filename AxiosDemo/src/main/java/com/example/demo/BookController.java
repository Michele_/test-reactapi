package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class BookController {

    @CrossOrigin(origins = "*",allowedHeaders = "*")
    @GetMapping("/books")
    public List< Book > getBooks() {
        return Arrays.asList(new Book(1, "Core Java","uncle bob"), new Book(2, "Effective Java","fraccazzo da velletri"), new Book(3, "Head First Java","ernesto sparalesto"));
    }
}