import logo from './logo.svg';
import './App.css';
import BookComponent from "./BookComponent";

function App() {
  return (
      <div>
        <header className="container">
          <BookComponent />
        </header>
      </div>
  );
}

export default App;
